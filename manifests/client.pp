class dyndns::client (
  $domain = undef,
  $bind_key = undef,
  $bind_key_algorithm = 'hmac-md5',
  $cname = ''
) {

  $nsupdate_key = "/etc/nsupdate/${dyndns::client::domain}.key"

  package { 'bind-utils':
    ensure  => present,
    allow_virtual => false,
  }

  file { '/etc/nsupdate':
    ensure  => directory,
    owner   => 'root',
    group   => 'root',
    mode    => '0750',
    require => Package['bind-utils'],
  }

  file {"$nsupdate_key":
    path    => "$nsupdate_key",
    content => template('dyndns/client/named.key.erb'),
    owner   => 'root',
    group   => 'root',
    mode    => '0440',
    require => File['/etc/nsupdate'],
  }

  exec { 'register forward dns' :
    command  => template('dyndns/client/register_dns.erb'),
    provider => 'shell',
    require  => File["$nsupdate_key"],
  }
  exec { 'register reverse dns' :
    command  => template('dyndns/client/register_reverse_dns.erb'),
    provider => 'shell',
    require  => File["$nsupdate_key"],
  }


}
